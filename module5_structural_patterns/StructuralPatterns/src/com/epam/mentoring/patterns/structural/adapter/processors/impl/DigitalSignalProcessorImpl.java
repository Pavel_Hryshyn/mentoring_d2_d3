package com.epam.mentoring.patterns.structural.adapter.processors.impl;

import com.epam.mentoring.patterns.structural.adapter.entity.Signal;
import com.epam.mentoring.patterns.structural.adapter.entity.SignalType;
import com.epam.mentoring.patterns.structural.adapter.processors.IDigitalSignalProcessor;

public class DigitalSignalProcessorImpl implements IDigitalSignalProcessor {

	@Override
	public Signal processDigitalSignal(Signal signal) {
		System.out.println("Processed digital signal");
		return signal;
	}

	@Override
	public Signal convertToAnalogSignal(Signal signal) {
		if (signal.getType() == SignalType.DIGITAL) {
			signal.setType(SignalType.ANALOG);
		}
		System.out.println("Converted digital signal to analog");
		return signal;
	}

}

package com.epam.mentoring.patterns.structural.bridge.model.generator;

public interface IGenerator {
	void energize();
}

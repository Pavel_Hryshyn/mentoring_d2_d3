package com.epam.mentoring.patterns.structural.adapter.processors;

import com.epam.mentoring.patterns.structural.adapter.entity.Signal;

public interface ISignalProcessor {
	Signal processSignal(Signal signal);
	
	Signal convertSignal(Signal signal);
}

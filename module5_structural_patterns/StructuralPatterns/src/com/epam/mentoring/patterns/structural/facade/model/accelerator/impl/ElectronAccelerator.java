package com.epam.mentoring.patterns.structural.facade.model.accelerator.impl;

import com.epam.mentoring.patterns.structural.facade.model.accelerator.IAccelerator;

public class ElectronAccelerator implements IAccelerator {

	@Override
	public void accelerate() {
		System.out.println("I accelerate electrons");
	}

}

package com.epam.mentoring.patterns.structural.adapter.processors;

import com.epam.mentoring.patterns.structural.adapter.entity.Signal;

public interface IAnalogSignalProcessor {
	Signal processAnalogSignal(Signal signal);
	
	Signal convertToDigitalSignal(Signal signal);
}

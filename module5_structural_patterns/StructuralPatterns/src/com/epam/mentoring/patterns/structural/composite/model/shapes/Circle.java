package com.epam.mentoring.patterns.structural.composite.model.shapes;

public class Circle implements IShape {

	private String color;
		
	public Circle(String color) {
		super();
		this.color = color;
	}
	


	@Override
	public void draw() {
		System.out.println("Drawing " + color + " circle");
	}

}

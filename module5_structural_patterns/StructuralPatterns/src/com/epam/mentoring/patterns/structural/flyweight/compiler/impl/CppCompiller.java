package com.epam.mentoring.patterns.structural.flyweight.compiler.impl;

import com.epam.mentoring.patterns.structural.flyweight.compiler.ICompiler;
import com.epam.mentoring.patterns.structural.flyweight.model.Code;

public class CppCompiller implements ICompiler {
	
	public CppCompiller() {
		super();
		System.out.println("C++ compiler was created");
	}

	@Override
	public void compile(Code code) {
		System.out.println("Code was compiled by Java compiler");
	}

}

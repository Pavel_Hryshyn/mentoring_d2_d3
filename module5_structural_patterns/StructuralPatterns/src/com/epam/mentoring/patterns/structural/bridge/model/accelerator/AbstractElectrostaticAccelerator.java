package com.epam.mentoring.patterns.structural.bridge.model.accelerator;

import com.epam.mentoring.patterns.structural.bridge.model.generator.IGenerator;

public abstract class AbstractElectrostaticAccelerator {
	protected IGenerator generator;

	public AbstractElectrostaticAccelerator(IGenerator generator) {
		super();
		this.generator = generator;
	}
	
	abstract public void accelerateBeam(); 
}

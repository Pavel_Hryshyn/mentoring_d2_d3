package com.epam.mentoring.patterns.structural.flyweight.compiler.factory;

import java.util.HashMap;
import java.util.Map;

import com.epam.mentoring.patterns.structural.flyweight.compiler.ICompiler;
import com.epam.mentoring.patterns.structural.flyweight.compiler.impl.CompilerType;
import com.epam.mentoring.patterns.structural.flyweight.compiler.impl.JavaCompiler;

public class CompilerFactory {
	private static Map<CompilerType, ICompiler> compilers = new HashMap<CompilerType, ICompiler>();

	public static ICompiler getCompiler(CompilerType type) {
		ICompiler compiler = compilers.get(type);
		if (compiler == null) {
			switch (type) {
			case JAVA:
				compiler = new JavaCompiler();
				break;
			case CPP:
				compiler = new JavaCompiler();
				break;
			}
			compilers.put(type, compiler);
		}
		return compiler;
	}
}

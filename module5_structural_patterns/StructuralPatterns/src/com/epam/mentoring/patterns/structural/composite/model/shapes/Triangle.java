package com.epam.mentoring.patterns.structural.composite.model.shapes;

public class Triangle implements IShape {

	private String color;
		
	public Triangle(String color) {
		super();
		this.color = color;
	}

	@Override
	public void draw() {
		System.out.println("Drawing " + color + " triangle");
	}

}

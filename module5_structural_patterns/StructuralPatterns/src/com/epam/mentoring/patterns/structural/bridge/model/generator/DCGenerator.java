package com.epam.mentoring.patterns.structural.bridge.model.generator;

public class DCGenerator implements IGenerator {

	@Override
	public void energize() {
		System.out.println("Generate direct current");
	}

}

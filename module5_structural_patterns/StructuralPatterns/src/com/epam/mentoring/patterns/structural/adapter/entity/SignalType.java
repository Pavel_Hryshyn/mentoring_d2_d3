package com.epam.mentoring.patterns.structural.adapter.entity;

public enum SignalType {
	ANALOG,
	DIGITAL;
}

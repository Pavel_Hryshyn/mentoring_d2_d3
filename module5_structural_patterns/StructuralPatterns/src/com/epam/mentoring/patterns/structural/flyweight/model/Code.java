package com.epam.mentoring.patterns.structural.flyweight.model;

public class Code {
	private String code;

	public Code(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}	
}

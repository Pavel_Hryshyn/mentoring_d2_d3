package com.epam.mentoring.patterns.structural.decorator.phone;

public class MobilePhoneDecorator implements IMobilePhone {

	private IMobilePhone mobilePhone;
		
	public MobilePhoneDecorator(IMobilePhone mobilePhone) {
		super();
		this.mobilePhone = mobilePhone;
	}

	@Override
	public void call() {
		mobilePhone.call();
	}

	@Override
	public void sendSMS() {
		mobilePhone.sendSMS();
	}

}

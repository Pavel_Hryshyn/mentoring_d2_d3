package com.epam.mentoring.patterns.structural.adapter.processors.adapters;

import com.epam.mentoring.patterns.structural.adapter.entity.Signal;
import com.epam.mentoring.patterns.structural.adapter.entity.SignalType;
import com.epam.mentoring.patterns.structural.adapter.processors.IAnalogSignalProcessor;
import com.epam.mentoring.patterns.structural.adapter.processors.IDigitalSignalProcessor;
import com.epam.mentoring.patterns.structural.adapter.processors.ISignalProcessor;
import com.epam.mentoring.patterns.structural.adapter.processors.impl.AnalogSignalProcessorImpl;
import com.epam.mentoring.patterns.structural.adapter.processors.impl.DigitalSignalProcessorImpl;

public class SignalProcessorAdaptor implements ISignalProcessor {

	private IAnalogSignalProcessor analogProcessor;
	
	private IDigitalSignalProcessor digitalProcessor;
	
	
	
	public SignalProcessorAdaptor() {
		super();
		this.analogProcessor = new AnalogSignalProcessorImpl();
		this.digitalProcessor = new DigitalSignalProcessorImpl();
	}

	public SignalProcessorAdaptor(IAnalogSignalProcessor analogProcessor,
			IDigitalSignalProcessor digitalProcessor) {
		super();
		this.analogProcessor = analogProcessor;
		this.digitalProcessor = digitalProcessor;
	}

	@Override
	public Signal processSignal(Signal signal) {
		if (signal.getType() == SignalType.ANALOG) {
			analogProcessor.processAnalogSignal(signal);
		}
		if (signal.getType() == SignalType.DIGITAL) {
			digitalProcessor.processDigitalSignal(signal);
		}
		return signal;
	}

	@Override
	public Signal convertSignal(Signal signal) {
		switch (signal.getType()) {
		case ANALOG:
			signal = analogProcessor.convertToDigitalSignal(signal);
			break;
		case DIGITAL:
			signal = digitalProcessor.convertToAnalogSignal(signal);
			break;
		}
		return signal;
	}

}

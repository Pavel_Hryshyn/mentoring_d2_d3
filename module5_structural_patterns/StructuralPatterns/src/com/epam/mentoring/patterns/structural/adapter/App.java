package com.epam.mentoring.patterns.structural.adapter;

import com.epam.mentoring.patterns.structural.adapter.entity.Signal;
import com.epam.mentoring.patterns.structural.adapter.entity.SignalType;
import com.epam.mentoring.patterns.structural.adapter.processors.ISignalProcessor;
import com.epam.mentoring.patterns.structural.adapter.processors.adapters.SignalProcessorAdaptor;

public class App {

	public static void main(String[] args) {
		Signal analogSignal = new Signal(SignalType.ANALOG, 10.0);
		Signal digitalSignal = new Signal(SignalType.DIGITAL, 5.8);
		ISignalProcessor adaptor = new SignalProcessorAdaptor();
		
		adaptor.processSignal(analogSignal);
		adaptor.processSignal(digitalSignal);
		adaptor.convertSignal(analogSignal);
		adaptor.convertSignal(digitalSignal);
	}

}

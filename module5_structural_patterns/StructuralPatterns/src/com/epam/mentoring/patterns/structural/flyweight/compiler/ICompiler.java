package com.epam.mentoring.patterns.structural.flyweight.compiler;

import com.epam.mentoring.patterns.structural.flyweight.model.Code;

public interface ICompiler {
	void compile(Code code);
}

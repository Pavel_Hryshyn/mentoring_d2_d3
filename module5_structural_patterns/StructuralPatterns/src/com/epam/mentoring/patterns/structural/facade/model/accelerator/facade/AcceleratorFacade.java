package com.epam.mentoring.patterns.structural.facade.model.accelerator.facade;

import com.epam.mentoring.patterns.structural.facade.model.accelerator.IAccelerator;
import com.epam.mentoring.patterns.structural.facade.model.accelerator.impl.ElectronAccelerator;
import com.epam.mentoring.patterns.structural.facade.model.accelerator.impl.HeavyIonAccelerator;
import com.epam.mentoring.patterns.structural.facade.model.accelerator.impl.ProtonAccelerator;

public class AcceleratorFacade {
	private IAccelerator electronAccelerator;
	private IAccelerator protonAccelerator;
	private IAccelerator heavyIonsAccelerator;
	
	public AcceleratorFacade() {
		super();
		this.electronAccelerator = new ElectronAccelerator();
		this.protonAccelerator = new ProtonAccelerator();
		this.heavyIonsAccelerator = new HeavyIonAccelerator();
	}
	
	public void accelerateElectrons() {
		electronAccelerator.accelerate();
	}
	
	public void accelerateProtons() {
		protonAccelerator.accelerate();
	}
	
	public void accelerateHeavyIons() {
		heavyIonsAccelerator.accelerate();
	}
}

package com.epam.mentoring.patterns.structural.composite.model;

import java.util.ArrayList;
import java.util.List;

import com.epam.mentoring.patterns.structural.composite.model.shapes.IShape;

public class Drawer implements IShape {

	List<IShape> shapes = new ArrayList<IShape>();
	
	@Override
	public void draw() {
		for (IShape shape: shapes) {
			shape.draw();
		}
	}
	
	public void add(IShape shape){
		shapes.add(shape);
	}
	
	public void clear(IShape shape) {
		shapes.clear();
	}

}

package com.epam.mentoring.patterns.structural.bridge.model.accelerator;

import com.epam.mentoring.patterns.structural.bridge.model.generator.IGenerator;

public class VanDeGraaffAccelerator extends AbstractElectrostaticAccelerator {

	public VanDeGraaffAccelerator(IGenerator generator) {
		super(generator);
	}

	@Override
	public void accelerateBeam() {
		System.out.println("Start belt");
		generator.energize();
	}

}

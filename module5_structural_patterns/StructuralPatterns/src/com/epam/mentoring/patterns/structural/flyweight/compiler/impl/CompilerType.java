package com.epam.mentoring.patterns.structural.flyweight.compiler.impl;

public enum CompilerType {
	JAVA,
	CPP;
}

package com.epam.mentoring.patterns.structural.composite.model.shapes;

public interface IShape {
	void draw();
}

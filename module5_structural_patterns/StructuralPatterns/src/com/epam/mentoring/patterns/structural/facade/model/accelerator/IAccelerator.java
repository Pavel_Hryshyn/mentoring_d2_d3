package com.epam.mentoring.patterns.structural.facade.model.accelerator;

public interface IAccelerator {
	void accelerate();
}

package com.epam.mentoring.patterns.structural.adapter.processors;

import com.epam.mentoring.patterns.structural.adapter.entity.Signal;

public interface IDigitalSignalProcessor {
	Signal processDigitalSignal(Signal signal);
	
	Signal convertToAnalogSignal(Signal signal);
}

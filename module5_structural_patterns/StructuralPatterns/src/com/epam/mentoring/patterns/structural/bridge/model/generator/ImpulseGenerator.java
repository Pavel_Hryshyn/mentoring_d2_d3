package com.epam.mentoring.patterns.structural.bridge.model.generator;

public class ImpulseGenerator implements IGenerator {

	@Override
	public void energize() {
		System.out.println("Generate impulse current");
	}

}

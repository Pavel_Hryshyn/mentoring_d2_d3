package com.epam.mentoring.patterns.structural.composite;

import com.epam.mentoring.patterns.structural.composite.model.Drawer;
import com.epam.mentoring.patterns.structural.composite.model.shapes.Circle;
import com.epam.mentoring.patterns.structural.composite.model.shapes.IShape;
import com.epam.mentoring.patterns.structural.composite.model.shapes.Triangle;

public class App {

	public static void main(String[] args) {
		IShape greenTriangle = new Triangle("green");
		IShape redTriangle = new Triangle("red");
		IShape blueCircle = new Circle("blue");
		IShape blackCircle = new Circle("black");
		Drawer drawer = new Drawer();
		
		drawer.add(greenTriangle);
		drawer.add(greenTriangle);
		drawer.add(redTriangle);
		drawer.add(blackCircle);
		drawer.add(blueCircle);
		drawer.add(greenTriangle);
		
		drawer.draw();
	}

}

package com.epam.mentoring.patterns.structural.decorator.phone.impl;

import com.epam.mentoring.patterns.structural.decorator.phone.IMobilePhone;
import com.epam.mentoring.patterns.structural.decorator.phone.MobilePhoneDecorator;

public class SmartPhone extends MobilePhoneDecorator {

	public SmartPhone(IMobilePhone mobilePhone) {
		super(mobilePhone);
	}

	@Override
	public void call() {
		super.call();
		System.out.println("I can do video call also");
		System.out.println("-----------------------------");
	}

	@Override
	public void sendSMS() {
		super.sendSMS();
		System.out.println("Also I can sent photo as sms");
		System.out.println("-----------------------------");
	}
}

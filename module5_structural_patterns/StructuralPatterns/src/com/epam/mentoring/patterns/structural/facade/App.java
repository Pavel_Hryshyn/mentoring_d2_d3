package com.epam.mentoring.patterns.structural.facade;

import com.epam.mentoring.patterns.structural.facade.model.accelerator.facade.AcceleratorFacade;

public class App {

	public static void main(String[] args) {
		AcceleratorFacade facade = new AcceleratorFacade();
		
		facade.accelerateElectrons();
		facade.accelerateHeavyIons();
		facade.accelerateProtons();

	}

}

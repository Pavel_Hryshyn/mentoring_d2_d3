package com.epam.mentoring.patterns.structural.decorator.phone;

public interface IMobilePhone {
	void call();
	
	void sendSMS();
	
}

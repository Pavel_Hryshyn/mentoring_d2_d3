package com.epam.mentoring.patterns.structural.adapter.entity;

public class Signal {

	private SignalType type;
	
	private Double amplitude;

	public Signal(SignalType type, Double amplitude) {
		super();
		this.type = type;
		this.amplitude = amplitude;
	}
	
	public SignalType getType() {
		return type;
	}



	public void setType(SignalType type) {
		this.type = type;
	}

	public Double getAmplitude() {
		return amplitude;
	}

	public void setAmplitude(Double amplitude) {
		this.amplitude = amplitude;
	}
}

package com.epam.mentoring.patterns.structural.decorator;

import com.epam.mentoring.patterns.structural.decorator.phone.IMobilePhone;
import com.epam.mentoring.patterns.structural.decorator.phone.impl.MobilePhone;
import com.epam.mentoring.patterns.structural.decorator.phone.impl.SmartPhone;

public class App {

	public static void main(String[] args) {
		IMobilePhone mobilePhone = new MobilePhone();
		IMobilePhone smartPhone = new SmartPhone(mobilePhone);
		
		smartPhone.call();
		smartPhone.sendSMS();
	}

}

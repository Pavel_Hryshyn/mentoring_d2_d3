package com.epam.mentoring.patterns.structural.bridge.model.accelerator;

import com.epam.mentoring.patterns.structural.bridge.model.generator.IGenerator;

public class CascadeAccelerator extends AbstractElectrostaticAccelerator {

	public CascadeAccelerator(IGenerator generator) {
		super(generator);
	}

	@Override
	public void accelerateBeam() {
		System.out.println("get voltage on cascade");
		generator.energize();
	}

}

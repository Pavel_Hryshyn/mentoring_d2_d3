package com.epam.mentoring.patterns.structural.flyweight;

import com.epam.mentoring.patterns.structural.flyweight.compiler.ICompiler;
import com.epam.mentoring.patterns.structural.flyweight.compiler.factory.CompilerFactory;
import com.epam.mentoring.patterns.structural.flyweight.compiler.impl.CompilerType;
import com.epam.mentoring.patterns.structural.flyweight.model.Code;

public class App {

	public static void main(String[] args) {
		Code code = new Code("Code for compilation");
		ICompiler javaCompiler = CompilerFactory.getCompiler(CompilerType.JAVA);
		javaCompiler.compile(code);
		ICompiler javaCompiler2 = CompilerFactory.getCompiler(CompilerType.JAVA);
		javaCompiler2.compile(code);
	}

}

package com.epam.mentoring.patterns.structural.adapter.processors.impl;

import com.epam.mentoring.patterns.structural.adapter.entity.Signal;
import com.epam.mentoring.patterns.structural.adapter.entity.SignalType;
import com.epam.mentoring.patterns.structural.adapter.processors.IAnalogSignalProcessor;

public class AnalogSignalProcessorImpl implements IAnalogSignalProcessor {

	@Override
	public Signal processAnalogSignal(Signal signal) {
		System.out.println("Processed analog signal");
		return signal;
	}

	@Override
	public Signal convertToDigitalSignal(Signal signal) {
		if (signal.getType() == SignalType.ANALOG) {
			signal.setType(SignalType.DIGITAL);
		}
		System.out.println("Converted analog signal to digital");
		return signal;
	}

}

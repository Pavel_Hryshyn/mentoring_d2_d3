package com.epam.mentoring.patterns.structural.decorator.phone.impl;

import com.epam.mentoring.patterns.structural.decorator.phone.IMobilePhone;

public class MobilePhone implements IMobilePhone {

	@Override
	public void call() {
		System.out.println("I can call to people");
	}

	@Override
	public void sendSMS() {
		System.out.println("I can send SMS to people");
	}

}

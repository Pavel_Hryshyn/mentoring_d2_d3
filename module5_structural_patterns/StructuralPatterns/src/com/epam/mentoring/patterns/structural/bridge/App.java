package com.epam.mentoring.patterns.structural.bridge;

import com.epam.mentoring.patterns.structural.bridge.model.accelerator.AbstractElectrostaticAccelerator;
import com.epam.mentoring.patterns.structural.bridge.model.accelerator.VanDeGraaffAccelerator;
import com.epam.mentoring.patterns.structural.bridge.model.generator.DCGenerator;
import com.epam.mentoring.patterns.structural.bridge.model.generator.IGenerator;

public class App {

	public static void main(String[] args) {
		IGenerator generator = new DCGenerator();
		AbstractElectrostaticAccelerator accelerator = new VanDeGraaffAccelerator(generator);
		accelerator.accelerateBeam();

	}

}

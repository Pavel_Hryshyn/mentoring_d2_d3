package com.epam.mentoring.patterns.structural.flyweight.compiler.impl;

import com.epam.mentoring.patterns.structural.flyweight.compiler.ICompiler;
import com.epam.mentoring.patterns.structural.flyweight.model.Code;

public class JavaCompiler implements ICompiler {

	public JavaCompiler() {
		super();
		System.out.println("Java compiler was created");
	}

	@Override
	public void compile(Code code) {
		System.out.println("Code was compiled by Java compiler");
	}

}

package com.epam.mentoring.railway.booking.service;

import java.util.HashMap;
import java.util.Map;

import javax.jws.WebService;

import com.epam.mentoring.railway.booking.model.Ticket;

@WebService(endpointInterface="com.epam.mentoring.railway.booking.service.BookingService")
public class BookingServiceImpl implements BookingService {

	private Map<Integer, Ticket> tickets = new HashMap<Integer, Ticket>();
	
	@Override
	public Integer bookTicket(Ticket ticket) {
		Integer id = (int)(Math.random()*Integer.MAX_VALUE);
		ticket.setId(id);
		tickets.put(id, ticket);
		return id;
	}

	@Override
	public Ticket getTicket(Integer id) {
		return tickets.get(id);
	}
	
	@Override
	public boolean payTicket(Integer id, Double amount) {
		Ticket ticket = tickets.get(id);
		boolean isPaid = false;
		if (ticket != null) {
			if (ticket.getCost() < amount) {
				isPaid = true;
				ticket.setPaid(isPaid);
				System.out.println("Ticket is paid succesfully! id = " + id);
			}
		} else {
			System.out.println("Ticket is not found");
		}
		return isPaid;
	}

	@Override
	public boolean returnTicket(Integer id) {
		boolean isDeleted = false;
		Ticket ticket = tickets.remove(id);
		if (ticket != null)  {
			isDeleted = true;
		}
		return isDeleted;
	}
}

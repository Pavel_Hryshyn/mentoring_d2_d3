package com.epam.mentoring.railway.booking.model;

import java.io.Serializable;

public class Ticket implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String arrivalStation;
	
	private String departureStation;
	
	private Double cost;
	
	private boolean isPaid;

	public Ticket() {
		super();
	}
	
	public Ticket(String arrivalStation, String departureStation, Double cost) {
		super();
		this.arrivalStation = arrivalStation;
		this.departureStation = departureStation;
		this.cost = cost;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}
	
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	public boolean isPaid() {
		return isPaid;
	}

	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	@Override
	public String toString() {
		return "Ticket [id=" + id + ", arrivalStation=" + arrivalStation
				+ ", departureStation=" + departureStation + ", cost=" + cost
				+ ", isPaid=" + isPaid + "]";
	}
}

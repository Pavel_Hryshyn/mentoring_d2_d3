package com.epam.mentoring.railway.booking.publisher;

import javax.xml.ws.Endpoint;

import com.epam.mentoring.railway.booking.service.BookingServiceImpl;


public class BookingPublisher {

	public static void main(String[] args) {
		Endpoint endpoint = Endpoint.publish("http://localhost:5674/ws/booking", new BookingServiceImpl());
		System.out.println(endpoint.isPublished());
	}
}

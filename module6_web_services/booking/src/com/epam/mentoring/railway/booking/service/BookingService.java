package com.epam.mentoring.railway.booking.service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import com.epam.mentoring.railway.booking.model.Ticket;

@WebService
@SOAPBinding(style=Style.RPC)
public interface BookingService {

	@WebMethod
	public Integer bookTicket(Ticket ticket);
	
	@WebMethod
	public Ticket getTicket(Integer id);
	
	@WebMethod
	public boolean payTicket(Integer id, Double amount);
	
	@WebMethod
	public boolean returnTicket(Integer id);
}

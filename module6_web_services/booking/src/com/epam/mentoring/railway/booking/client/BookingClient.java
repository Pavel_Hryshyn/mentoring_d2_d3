package com.epam.mentoring.railway.booking.client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import com.epam.mentoring.railway.booking.model.Ticket;
import com.epam.mentoring.railway.booking.service.BookingService;



public class BookingClient {

	public static void main(String[] args) throws MalformedURLException {
		URL url = new URL("http://localhost:5674/ws/booking?wsdl");

		QName qname = new QName("http://service.booking.railway.mentoring.epam.com/", "BookingServiceImplService"); 
		Service service = Service.create(url, qname);  
		BookingService bookingService = service.getPort(BookingService.class);
		
		Ticket ticket1 = new Ticket("Minsk", "Warsaw", 105.5);
		Ticket ticket2 = new Ticket("Minsk", "Vitebsk", 15.5);
		Ticket ticket3 = new Ticket("Minsk", "Gomel", 12.5);
		
		Integer id1 = bookingService.bookTicket(ticket1);
		Integer id2 = bookingService.bookTicket(ticket2);
		Integer id3 = bookingService.bookTicket(ticket3);
		System.out.println("id1 = " + id1);
		System.out.println("id2 = " + id2);
		System.out.println("id3 = " + id3);
		System.out.println(bookingService.getTicket(id1));
		System.out.println(bookingService.getTicket(id2));
		System.out.println(bookingService.getTicket(id3));
		System.out.println("Ticket1: payment status " + bookingService.payTicket(id1, 50.5));
		System.out.println("Ticket1: payment status " + bookingService.payTicket(id2, 50.5));
		System.out.println("Ticket1: payment status " + bookingService.payTicket(id3, 50.5));
		System.out.println("Ticket1: returning status " + bookingService.returnTicket(id1));
	}

}

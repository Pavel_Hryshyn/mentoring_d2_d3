package com.epam.mentoring.patterns.consumer;

import com.epam.mentoring.patterns.model.Person;

public interface IConsumer {
	
	Person getPerson(String name);
	
	Person updatePerson(Person person);
}

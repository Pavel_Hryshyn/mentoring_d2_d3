package com.epam.mentoring.patterns.consumer.impl;

import com.epam.mentoring.patterns.consumer.IConsumer;
import com.epam.mentoring.patterns.model.Person;

public class FileConsumerWithLazyInit implements IConsumer {
	private static FileConsumerWithLazyInit instance;
	
	private FileConsumerWithLazyInit() {
		super();
		System.out.println("create instance of FileConsumerWithLazyInit");
	}

	public static FileConsumerWithLazyInit getInstance() {
		if (instance == null) {
			instance = new FileConsumerWithLazyInit();
		}
		return instance;
	}

	@Override
	public Person getPerson(String name) {
		Person person = new Person(name, "Lazy init From file");
		return person;
	}

	@Override
	public Person updatePerson(Person person) {
		return person;
	}
	
	
}

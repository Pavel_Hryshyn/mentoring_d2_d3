package com.epam.mentoring.patterns.factory;

import com.epam.mentoring.patterns.consumer.IConsumer;

public interface IConsumerCreator {
	IConsumer createConsumer();
}

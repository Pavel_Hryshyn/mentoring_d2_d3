package com.epam.mentoring.patterns.consumer.impl;

import com.epam.mentoring.patterns.consumer.IConsumer;
import com.epam.mentoring.patterns.model.Person;

public enum FileConsumerWithEagerInit implements IConsumer {
	INSTANSE;

	static {
		System.out.println("create instance of FileConsumerWithEagerInit");
	}
	
	@Override
	public Person getPerson(String name) {
		Person person = new Person(name, "Eager iniy from file");
		return person;
	}

	@Override
	public Person updatePerson(Person person) {
		return person;	
	}

	
}

package com.epam.mentoring.patterns.factory.impl;

import com.epam.mentoring.patterns.consumer.IConsumer;
import com.epam.mentoring.patterns.consumer.impl.FileConsumerWithEagerInit;
import com.epam.mentoring.patterns.factory.IConsumerCreator;

public class FileConsumerWithEagerInitCreator implements IConsumerCreator {

	private String fileName;
		
	public FileConsumerWithEagerInitCreator(String fileName) {
		super();
		this.fileName = fileName;
	}

	@Override
	public IConsumer createConsumer() {
		return FileConsumerWithEagerInit.INSTANSE;
	}

	public String getFileName() {
		return fileName;
	}
}

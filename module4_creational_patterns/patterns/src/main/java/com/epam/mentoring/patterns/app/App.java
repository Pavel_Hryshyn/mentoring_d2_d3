package com.epam.mentoring.patterns.app;

import com.epam.mentoring.patterns.consumer.IConsumer;
import com.epam.mentoring.patterns.factory.IConsumerCreator;
import com.epam.mentoring.patterns.factory.impl.FileConsumerWithEagerInitCreator;
import com.epam.mentoring.patterns.factory.impl.FileConsumerWithLazyInitCreator;

public class App {

	public static void main(String[] args) {
		
		IConsumerCreator eager = new FileConsumerWithEagerInitCreator("path");
		IConsumerCreator lazy = new FileConsumerWithLazyInitCreator("lazy path");
		IConsumer eagerConsumer = eager.createConsumer();
		System.out.println(eagerConsumer.getPerson("lastName"));
		IConsumer lazyConsumer = lazy.createConsumer();
		System.out.println(lazyConsumer.getPerson("lazy lastName"));
	}

}

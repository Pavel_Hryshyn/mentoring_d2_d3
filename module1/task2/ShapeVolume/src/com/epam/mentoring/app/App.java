package com.epam.mentoring.app;

import java.util.ArrayList;
import java.util.List;

import com.epam.mentoring.figure.shape.AbstractFigure;
import com.epam.mentoring.figure.shape.ComplexFigure;
import com.epam.mentoring.figure.shape.RectangularPrism;
import com.epam.mentoring.figure.shape.Sphere;
import com.epam.mentoring.figure.shape.TriangularPrism;

public class App {
	
	public static void main(String[] args) {
		AbstractFigure sphere = new Sphere(10.0, 2.0);
		AbstractFigure prism = new RectangularPrism(5.0, 3.0, 7.0);
		AbstractFigure triangularPrism = new TriangularPrism(10.0, 5.0);
		List<AbstractFigure> figures = new ArrayList<AbstractFigure>();
		figures.add(sphere);
		figures.add(prism);
		figures.add(prism);
		figures.add(triangularPrism);
		AbstractFigure complexFigure = new ComplexFigure(figures);
		System.out.println("Sphere volume = " + sphere.calculateVolume());
		System.out.println("Prism volume = " + prism.calculateVolume());
		System.out.println("Triangular prism volume = " + triangularPrism.calculateVolume());
		System.out.println("Complex figure volume = " + complexFigure.calculateVolume());
	}
}

/*
 * ComplexFigure.java
 */
package com.epam.mentoring.figure.shape;

import java.util.List;


/**
 * Defines methods for Complex figure
 * @author Pavel Hryshyn
 *
 */
public class ComplexFigure extends AbstractFigure {

	private List<AbstractFigure> figures;
			
	public ComplexFigure() {
		super();
	}

	public ComplexFigure(List<AbstractFigure> figures) {
		super();
		this.figures = figures;
	}

	@Override
	public Double calculateVolume() {
		volume = 0.0;
		if (figures != null) {
			for (AbstractFigure figure: figures) {
				volume += figure.calculateVolume();
			}
		}
		return volume;
	}

	public List<AbstractFigure> getFigures() {
		return figures;
	}

	public void setFigures(List<AbstractFigure> figures) {
		this.figures = figures;
	}
}

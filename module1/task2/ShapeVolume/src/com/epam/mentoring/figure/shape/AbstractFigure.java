/*
 * AbstractShape
 */
package com.epam.mentoring.figure.shape;


/**
 * Abstract class for Figures
 * @author Pavel Hryshyn
 *
 */
public abstract class AbstractFigure {

	protected Double volume;
	
	public AbstractFigure() {
		super();
	}

	/**
	 * Calculates volume of Figure
	 * @return volume
	 */
	public abstract Double calculateVolume();
	
	public Double getVolume() {
		return volume;
	}
}

/*
 * Sphere.java
 */
package com.epam.mentoring.figure.shape;

/**
 * 
 * @author Pavel Hryshyn
 *
 */
public class Sphere extends AbstractFigure {
	private Double radius;

	private Double denaminator;
		
	public Sphere() {
		super();
	}

	public Sphere(Double radius) {
		super();
		this.radius = radius;
	}

	public Sphere(Double radius, Double denaminator) {
		super();
		this.radius = radius;
		this.denaminator = denaminator;
	}

	@Override
	public Double calculateVolume() {
		if (denaminator == null || denaminator <1){
			denaminator = 1.0;
		}
		volume = (4*Math.PI*Math.pow(radius, 3))/(3*denaminator);
		return volume;
	}

	public Double getRadius() {
		return radius;
	}

	public void setRadius(Double radius) {
		this.radius = radius;
	}

	public Double getDenaminator() {
		return denaminator;
	}
}

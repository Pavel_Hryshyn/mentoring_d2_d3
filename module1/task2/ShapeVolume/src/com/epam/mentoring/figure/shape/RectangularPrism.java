/*
 * Rectangle.java
 */
package com.epam.mentoring.figure.shape;

/**
 * 
 * @author Pavel Hryshyn
 *
 */
public class RectangularPrism extends AbstractFigure {

	private Double length;
	
	private Double width;
	
	private Double height;

	public RectangularPrism(Double length, Double width, Double height) {
		super();
		this.length = length;
		this.width = width;
		this.height = height;
	}
	
	public RectangularPrism() {
		super();
	}
	
	@Override
	public Double calculateVolume() {
		volume = length*height*width;
		return volume;
	}

	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	@Override
	public String toString() {
		return "RectangularPrism [length=" + length + ", width=" + width + ", height="
				+ height + "]";
	}
}

/*
 * TriangularPrism.java
 */
package com.epam.mentoring.figure.shape;

/**
 * 
 * @author Pavel Hryshyn
 *
 */
public class TriangularPrism extends AbstractFigure {

	private Double baseArea;
	
	private Double height;

	public TriangularPrism(Double baseArea, Double height) {
		super();
		this.baseArea = baseArea;
		this.height = height;
	}

	public TriangularPrism() {
		super();
	}
	
	@Override
	public Double calculateVolume() {
		volume = (baseArea*height)/3;
		return volume;
	}

	public Double getBaseArea() {
		return baseArea;
	}

	public void setBaseArea(Double baseArea) {
		this.baseArea = baseArea;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}
}

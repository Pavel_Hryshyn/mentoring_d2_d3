package excel.merger.merger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import excel.merger.entities.Record;

/**
 * Merges two the same columns name .xlsx files
 * @author Pavel_Hryshyn
 *
 */
public class MergerForSameFiles implements IMerger {
	private final static Logger logger = Logger.getLogger(MergerForSameFiles.class);
	int index = 0;
	
	public List<Record> merge(List<Record> from, List<Record> to) {
		Map<String, Record> mergedMap = new LinkedHashMap<String, Record>();
		List<Record> mergedRecords = null;
		logger.info("Merging files started");
		
		for (Record fromRecord: from) {
			if (fromRecord.getPrimaryKey()!= null){
				mergedMap.put(fromRecord.getPrimaryKey(), fromRecord); 
			}	
		}
		
		for (Record toRecord: to){
			
			if (toRecord.getPrimaryKey()!= null && !mergedMap.containsKey(toRecord.getPrimaryKey())){
				logger.info("Add Object = " + toRecord);
				mergedMap.put(toRecord.getPrimaryKey(), toRecord); 
				index++;
			}	
			
		}
		mergedRecords = new ArrayList<Record>(mergedMap.values());
		
		logger.info("Merging files finished");
		return mergedRecords;
	}

	@Override
	public List<Record> merge(List<Record> from, List<Record> to,
			int[] mergedIndexes) {
		throw new UnsupportedOperationException("The method is unsupported");
	}
	
	
}

package excel.merger.merger;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import excel.merger.entities.Record;

/**
 * Merges two .xlsx files
 * @author Pavel_Hryshyn
 *
 */
public class XLSXMerger implements IMerger {
	private final static Logger logger = Logger.getLogger(XLSXMerger.class);
	
	List<Record> sameRecords = new ArrayList<Record>(); 
	
	@Override
	public List<Record> merge(List<Record> from, List<Record> to,
			int[] mergedIndexesFrom) {
		logger.info("Merging files started");
		List<Record> mergedRecords = new ArrayList<Record>(to);
		System.out.println(from.size());
		for (Record recordFrom: from) {
			findRecordForMerge(recordFrom, mergedRecords, mergedIndexesFrom);
		}

		logger.info("Merging files finished");
		return mergedRecords;
	}
	
	
	@Override
	public List<Record> merge(List<Record> from, List<Record> to) {
		throw new UnsupportedOperationException("The method is unsupported");
	}



	private void findRecordForMerge(Record from, List<Record> to, int[] mergedIndexes) {
		for (Record merged: to) {
			
			if (merged.getPrimaryKey() != null && from.getPrimaryKey() != null) {
				if (merged.getPrimaryKey().equalsIgnoreCase(from.getPrimaryKey())){
					int size = merged.getAttributes().size();

					for (int index: mergedIndexes) {
						merged.getAttributes().put(size, from.getAttributes().get(index));
						size++;
					}
			}
			}	
		}
	}
}

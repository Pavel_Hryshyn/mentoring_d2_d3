package excel.merger.merger;

import java.util.List;

import excel.merger.entities.Record;

/**
 * Defines methods for all mergers
 * @author Pavel_Hryshyn
 */
public interface IMerger {
	/**
	 * Merges two list using mergedIndexes
	 * @param from list from data merged
	 * @param to list to data merged
	 * @param mergedIndexes indexes columns from Record 
	 * 			using in merging   
	 * @return list of merged records
	 */
	List<Record> merge(List<Record> from, List<Record> to, int[] mergedIndexes);
	
	/**
	 * Merges two list using mergedIndexes
	 * @param from list from data merged
	 * @param to list to data merged
	 * @return list of merged records
	 */
	List<Record> merge(List<Record> from, List<Record> to);
}

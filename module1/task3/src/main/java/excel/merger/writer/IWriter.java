package excel.merger.writer;

import java.util.List;

import excel.merger.entities.Record;

/**
 * Defines methods for all writers
 * @author Pavel_Hryshyn
 *
 */
public interface IWriter {
	/**
	 * Write Record list to stream
	 * @param records
	 */
	void write(List<Record> records);
}

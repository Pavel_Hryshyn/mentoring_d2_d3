package excel.merger.readers;

import java.util.List;

import excel.merger.entities.Record;

/**
 * Defines methods to all readers
 * @author Pavel_Hryshyn
 *
 */
public interface IReader {
	/**
	 * Read data and transform to Record list
	 * @param file file name
	 * @param pkPosition primary key position
	 * @param lastIndex index of the last column
	 * @return list of Records
	 */
	List<Record> read(String file, Integer pkPosition, int lastIndex);
}

package excel.merger.app;

import java.util.List;

import excel.merger.entities.Record;
import excel.merger.merger.IMerger;
import excel.merger.merger.MergerForSameFiles;
import excel.merger.merger.XLSXMerger;
import excel.merger.readers.IReader;
import excel.merger.readers.XLSXReader;
import excel.merger.writer.IWriter;
import excel.merger.writer.XLSXWriter;

public class App {

	public static void main(String[] args) {
		
		String fileTo = "path to";
		String fileFrom = "path from";
		IReader readerTo = new XLSXReader();
		List<Record> recordsTo = readerTo.read(fileTo, 1, 30);
		List<Record> recordsFrom = readerTo.read(fileFrom, 0, 1);

		// Merge different files
		int[] gg = new int[] {1};
		IMerger merger = new XLSXMerger();
		List<Record> merged = merger.merge(recordsFrom, recordsTo, gg);
		
		
		//Merge same files
		IMerger mergerForSame = new MergerForSameFiles();
		List<Record> mergedForSame = mergerForSame.merge(recordsFrom, recordsTo);	
		
		IWriter writer1 = new XLSXWriter("merged.xlsx");
		writer1.write(merged);
		
		IWriter writer2 = new XLSXWriter("mergedForDame.xlsx");
		writer2.write(mergedForSame);
	}

}

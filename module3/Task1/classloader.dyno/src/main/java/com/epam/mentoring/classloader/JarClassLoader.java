/*
 *  JarClassLoader.class
 */
package com.epam.mentoring.classloader;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.log4j.Logger;

/**
 * Load classes from JAR archive
 * 
 * @author Pavel_Hryshyn
 *
 */
public class JarClassLoader extends ClassLoader {
	private static Logger logger = Logger.getLogger(JarClassLoader.class);

	private String pathToJar;

	private Map<String, Class<?>> cash = new HashMap<String, Class<?>>();

	public JarClassLoader(String pathToJar) {
		super();
		this.pathToJar = pathToJar;
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		byte[] classByte = null;
		Class result = null;
		String filename = name.replace(".", "/") + ".class";
		result = (Class) cash.get(name);
		if (result != null) {
			logger.info("Class " + name + " returns from cash");
			return result;
		}

		try {
			JarFile jar = new JarFile(pathToJar);
			if (!name.startsWith("java.lang")) {
				JarEntry entry = jar.getJarEntry(filename);
				InputStream stream = jar.getInputStream(entry);
				int size = stream.available();
				classByte = new byte[size];
				DataInputStream in = new DataInputStream(stream);
				in.readFully(classByte);
				in.close();
				jar.close();
				result = defineClass(name, classByte, 0, classByte.length);
				resolveClass(result);
				cash.put(name, result);
			}
		} catch (IOException e) {
			logger.error("Jar file didn't find", e);
		}
		return result;
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class loadedClass = findClass(name);
		if (loadedClass != null) {
			logger.info("Class loaded: " + name);
			return loadedClass;
		}
		return super.loadClass(name);
	}

}

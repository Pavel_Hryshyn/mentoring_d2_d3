package com.epam.mentoring.app;

import java.util.Scanner;

import org.apache.log4j.Logger;

import com.epam.mentoring.classloader.JarClassLoader;

public class App {
	private static Logger logger = Logger.getLogger(App.class);
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String pathToJar = null;
		logger.info("Enter path to jar: ");
		pathToJar = sc.nextLine();
		JarClassLoader classLoader = new JarClassLoader(pathToJar);
		
		boolean isContinued = true;
		
		while (isContinued) {
			String className = null;
			Class loadedClass = null;
			logger.info("Enter class name: ");
			className = sc.nextLine();
			try {
				loadedClass = classLoader.loadClass(className);
			} catch (ClassNotFoundException e) {
				logger.error("Error in loading time", e);
			}
		}
		sc.close();

	}

}

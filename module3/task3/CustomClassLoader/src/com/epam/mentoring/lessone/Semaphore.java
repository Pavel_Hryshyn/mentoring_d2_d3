package com.epam.mentoring.lessone;

/**
 * 
 * @author Pavel Hryshyn
 *
 */
public class Semaphore {

	public void lever() {
		System.out.println("It works!");
	}
	
}

package com.epam.mentoring.runner;



import java.util.Scanner;

import com.epam.mentoring.classloader.CustomClassLoader;
import com.epam.mentoring.invoker.MethodInvoker;

public class App {

	public static void main(String args[]) {
		ClassLoader parent = App.class.getClassLoader();
		CustomClassLoader classLoader = new CustomClassLoader(parent);
		MethodInvoker invoker = new MethodInvoker();
		Class loadedClass = null;
		
		Scanner sc = new Scanner(System.in);
		String input = null;
		System.out.println("Enter full class name: ");
		input = sc.nextLine();
		sc.close();
		
		try {
			loadedClass = classLoader.loadClass(input);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		invoker.invokeMethods(loadedClass);
		
	}
}

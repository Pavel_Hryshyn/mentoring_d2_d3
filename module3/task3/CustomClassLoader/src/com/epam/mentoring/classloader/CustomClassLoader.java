package com.epam.mentoring.classloader;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 
 * @author Pavel Hryshyn
 *
 */
public class CustomClassLoader extends ClassLoader {

	private final static String BASE_PACKAGE = "com.epam";
	
	public CustomClassLoader() {
		this(CustomClassLoader.class.getClassLoader());
	}

	public CustomClassLoader(ClassLoader parent) {
		super(parent);
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		Class loadedClass = findClass(name);
		if (loadedClass != null) {
			System.out.println("Loading status: success");
			return loadedClass;
		}
		return super.loadClass(name);
	}

	@Override
	protected Class<?> findClass(String name) throws ClassNotFoundException {
		byte b[] = null;
		Class c = null;
		try {
			String filename = name.replace(".", "/") + ".class";
			if (name.startsWith(BASE_PACKAGE)) {
				b = loadClassFileData(filename);
				c = defineClass(name, b, 0, b.length);
				resolveClass(c);
			}
		} catch (IOException e) {
			System.out.println("Loading status: error");
			e.printStackTrace();
		}
		return c;
	}

	private byte[] loadClassFileData(String name) throws IOException {
		System.out.println("Loading status: process");
		InputStream stream = getClass().getClassLoader().getResourceAsStream(
				name);
		int size = stream.available();
		byte buff[] = new byte[size];
		DataInputStream in = new DataInputStream(stream);
		in.readFully(buff);
		in.close();
		return buff;
	}
}

package com.epam.mentoring.invoker;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 
 * @author Pavel Hryshyn
 *
 */
public class MethodInvoker {

	public void invokeMethods(Class clazz) {
		Object obj;
		try {
			obj = clazz.newInstance();
			Method[] methods = clazz.getDeclaredMethods();
			for (Method method: methods) {
				method.invoke(obj, null);
			}
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}

package com.epam.mentoring.deadlock.app;

import com.epam.mentoring.deadlock.thread.Thread1;
import com.epam.mentoring.deadlock.thread.Thread2;

public class App {

	public static void main(String[] args) {
		Object monitor1 = new Object();
		Object monitor2 = new Object();
		
		Thread1 thread1 = new Thread1(monitor1, monitor2);
		Thread2 thread2 = new Thread2(monitor1, monitor2);
		
		thread1.start();
		thread2.start();
	}
}

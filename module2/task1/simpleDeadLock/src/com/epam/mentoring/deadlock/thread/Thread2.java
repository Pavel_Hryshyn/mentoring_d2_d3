package com.epam.mentoring.deadlock.thread;

public class Thread2 extends Thread {
	private Object monitor1;
	
	private Object monitor2;

	public Thread2(Object monitor1, Object monitor2) {
		super();
		this.monitor1 = monitor1;
		this.monitor2 = monitor2;
	}

	@Override
	public void run() {
		System.out.println("Start Thread2");
		synchronized (monitor2) {
			System.out.println("Thread2 locks monitor2");
			
			synchronized (monitor1) {
				System.out.println("Thread2 locks monitor1");
			}
		}
		System.out.println("Finish Thread2");	
	}
}

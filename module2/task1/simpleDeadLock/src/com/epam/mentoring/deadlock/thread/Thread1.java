package com.epam.mentoring.deadlock.thread;

public class Thread1 extends Thread {
	
	private Object monitor1;
	
	private Object monitor2;

	public Thread1(Object monitor1, Object monitor2) {
		super();
		this.monitor1 = monitor1;
		this.monitor2 = monitor2;
	}

	@Override
	public void run() {
		System.out.println("Start Thread1");
		synchronized (monitor1) {
			System.out.println("Thread1 locks monitor1");
			
			synchronized (monitor2) {
				System.out.println("Thread1 locks monitor2");
			}
		}
		System.out.println("Finish Thread1");	
	}
	
	
}
